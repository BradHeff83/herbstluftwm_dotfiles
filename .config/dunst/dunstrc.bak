[global]
    font = Terminus (TTF) 12
    
    # Allow a small subset of html markup:
    #   <b>bold</b>
    #   <i>italic</i>
    #   <s>strikethrough</s>
    #   <u>underline</u>

    markup = yes
    
    # The format of the message.  Possible variables are:
    #   %a  appname
    #   %s  summary
    #   %b  body
    #   %i  iconname (including its path)
    #   %I  iconname (without its path)
    #   %p  progress value if set ([  0%] to [100%]) or nothing
    # Markup is allowed
    format = "<span fgcolor='#73b4d4'><b><u>%a</u></b></span>\n<b>%s</b>\n%b\n%p"
    
    # Sort messages by urgency.
    sort = yes
    
    # Show how many messages are currently hidden (because of geometry).
    indicate_hidden = yes
    
    # Possible values are "left", "center" and "right".
    alignment = center
    
    # The frequency with wich text that is longer than the notification
    # window allows bounces back and forth.
    # This option conflicts with "word_wrap".
    # Set to 0 to disable.
    bounce_freq = 0
    
    # Show age of message if message is older than show_age_threshold
    # seconds.
    # Set to -1 to disable.
    show_age_threshold = 60
    
    # Split notifications into multiple lines if they don't fit into
    # geometry.
    word_wrap = yes
    
    # Ignore newlines '\n' in notifications.
    ignore_newline = no
    
    geometry = "300x5-30+20"
    
    # Shrink window if it's smaller than the width.  Will be ignored if
    # width is 0.
    shrink = no
    
    transparency = 20
    
    # Don't remove messages, if the user is idle (no mouse or keyboard input)
    # for longer than idle_threshold seconds.
    # Set to 0 to disable.
    idle_threshold = 120
    
    # Which monitor should the notifications be displayed on.
    monitor = 0
    
    # Display notification on focused monitor.  Possible modes are:
    #   mouse: follow mouse pointer
    #   keyboard: follow window with keyboard focus
    #   none: don't follow anything
    follow = none
    
    # Should a notification popped up from history be sticky or timeout
    # as if it would normally do.
    sticky_history = yes
    
    # Maximum amount of notifications kept in history
    history_length = 20
    
    # Display indicators for URLs (U) and actions (A).
    show_indicators = yes
    
    # The height of a single line.  If the height is smaller than the
    # font height, it will get raised to the font height.
    # This adds empty space above and under the text.
    line_height = 0
    
    # Draw a line of "separatpr_height" pixel height between two
    # notifications.
    # Set to 0 to disable.
    separator_height = 2
    
    # Padding between text and separator.
    padding = 8
    
    # Horizontal padding.
    horizontal_padding = 8
    
    # possible values are:
    #  * auto: dunst tries to find a color fitting to the background;
    #  * foreground: use the same color as the foreground;
    #  * frame: use the same color as the frame;
    #  * anything else will be interpreted as a X color.
    separator_color = auto
    
    startup_notification = false
    
    # dmenu path.
    dmenu = /usr/bin/dmenu -p dunst:
    
    # Browser for opening urls in context menu.
    browser = /usr/bin/firefox -new-tab

    # Align icons left/right/off
    icon_position = off

    # Paths to default icons.
    icon_path = /usr/share/icons/gnome/16x16/status/:/usr/share/icons/gnome/16x16/devices/

    frame_width = 3
    frame_color = "#fa9716"

[shortcuts]

    # Close notification.
    close = ctrl+space
    
    # Close all notifications.
    close_all = ctrl+shift+space
    
    history = ctrl+grave
    
    # Context menu.
    context = ctrl+shift+period

[urgency_low]
    # IMPORTANT: colors have to be defined in quotation marks.
    # Otherwise the "#" and following would be interpreted as a comment.
    background = "#23262f"
    foreground = "#ffffff"
    timeout = 10

[urgency_normal]
    background = "#23262f"
    foreground = "#ffffff"
    timeout = 10

[urgency_critical]
    background = "#23262f"
    foreground = "#ffffff"
    frame_color = "#c5241a"
    timeout = 0

[irc]
    appname = "weechat"
    format  = "%s: %b"
    urgency = low
    background = "#232627"
    foreground = "#ffffff"

#
# SCRIPTING
# You can specify a script that gets run when the rule matches by
# setting the "script" option.
# The script will be called as follows:
#   script appname summary body icon urgency
# where urgency can be "LOW", "NORMAL" or "CRITICAL".
# 
# NOTE: if you don't want a notification to be displayed, set the format
# to "".
# NOTE: It might be helpful to run dunst -print in a terminal in order
# to find fitting options for rules.

#[espeak]
#    summary = "*"
#    script = dunst_espeak.sh

#[script-test]
#    summary = "*script*"
#    script = dunst_test.sh

#[ignore]
#    # This notification will not be displayed
#    summary = "foobar"
#    format = ""

#[signed_on]
#    appname = Pidgin
#    summary = "*signed on*"
#    urgency = low
#
#[signed_off]
#    appname = Pidgin
#    summary = *signed off*
#    urgency = low
#
#[says]
#    appname = Pidgin
#    summary = *says*
#    urgency = critical
#
#[twitter]
#    appname = Pidgin
#    summary = *twitter.com*
#    urgency = normal
#
# vim: ft=cfg
